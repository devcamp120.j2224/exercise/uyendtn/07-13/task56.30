package com.devcamp.countryregion.service;

import java.util.ArrayList;

import com.devcamp.countryregion.model.Region;

import org.springframework.stereotype.Service;

@Service
public class RegionService {  
    Region hanoi = new Region("HN", "Ha Noi");
    Region tpHoChiMinh = new Region("HCM", "Thanh pho Ho Chi Minh");
    Region danang = new Region("DN", "Da Nang");

    Region newyorks = new Region("NY", "New Yorks");
    Region florida = new Region("FLO", "Florida");
    Region texas = new Region("TX", "Texas");

    Region moscow = new Region("MC", "Moscow");
    Region kaluga = new Region("KL", "Kaluga");
    Region saintpeterburg = new Region("SP", "Saint Peterburg");
       
    public ArrayList<Region> getRegionVietNam() {
        ArrayList<Region> regionVietNam = new ArrayList<Region>();
        
        regionVietNam.add(hanoi);
        regionVietNam.add(tpHoChiMinh);
        regionVietNam.add(danang);

        return regionVietNam;
    }

    public ArrayList<Region> getRegionUS() {
        ArrayList<Region> regionUS = new ArrayList<>();
        
        regionUS.add(newyorks);
        regionUS.add(florida);
        regionUS.add(texas);

        return regionUS;
    }

    public ArrayList<Region> getRegionRussia() {
        ArrayList<Region> regionRussia = new ArrayList<>();
        
        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(saintpeterburg);

        return regionRussia;
    }

    public ArrayList<Region> getAllRegion() {
        ArrayList<Region> regions = new ArrayList<>();
        
        regions.add(hanoi);
        regions.add(tpHoChiMinh);
        regions.add(danang);
        regions.add(newyorks);
        regions.add(florida);
        regions.add(texas);
        regions.add(moscow);
        regions.add(kaluga);
        regions.add(saintpeterburg);

        return regions;
    }

}
